package identify_map;

import entity.Student;

interface IOperations {
    Student getStudent(int id);
    void addStudent(Student student);
}
