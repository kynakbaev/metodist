package identify_map;

import data_base.DBconnection;
import data_mapper.StudentMapper;
import entity.Student;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @autor Kunakbaev Artem
 * Буферная коллекция объектов, позволяющая экономить подключения к базе данных,
 * если объект уже был один раз извлечен
 */
class IdentifyMap implements IOperations {
    private Map<Integer, Student> personMap = new HashMap<>();

    public void addStudent(Student student) {
        personMap.put(student.getId(), student);
        StudentMapper studentMapper = new StudentMapper(DBconnection.getConn());
        try {
            studentMapper.insert(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Student getStudent(int id) {
        if (personMap.containsKey(id)) {
            return personMap.get(id);
        } else {
            StudentMapper studentMapper = new StudentMapper(DBconnection.getConn());
            try {
                return studentMapper.findById(id);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }


}
