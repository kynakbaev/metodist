package identify_map;

import entity.Student;

/**
 * @autor Kunakbaev Artem
 * По сути это фасад коллекции объектов IdentifyMap
 */
public class FinderStudents implements IOperations {
    private IdentifyMap identifyMap = new IdentifyMap();


    @Override
    public Student getStudent(int id) {
        return identifyMap.getStudent(id);
    }

    @Override
    public void addStudent(Student student) {
        identifyMap.addStudent(student);
    }
}
