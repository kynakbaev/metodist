package entity;

import java.util.List;

/**
 * @autor Kunakbaev Artem
 */
public class Group {
    private String shortName;
    private String fullName;
    private List<Student> students;
    private Profession learningProfession;
}
