package entity;

import java.util.HashMap;
import java.util.Map;

/**
 * @autor Kunakbaev Artem
 */
public class Profession {
    private int code;
    private String name;
    private Map<Integer, String> professions = new HashMap<>();

    public Profession(int codeProf) {
        initProfessionsMap();
        this.code = codeProf;
        name = professions.get(code);
    }

    private void initProfessionsMap() {
        professions.put(1,	"Слесарь");
        professions.put(2,	"Электрик");
        professions.put(3,	"Делопроизводитель");
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return  name;
    }
}
