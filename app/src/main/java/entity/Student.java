package entity;

/**
 * @autor Kunakbaev Artem
 */
public class Student {
    private int id;
    private String name;
    private Profession wantedProfession;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Profession getWantedProfession() {
        return wantedProfession;
    }

    public void setWantedProfession(Profession wantedProfession) {
        this.wantedProfession = wantedProfession;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", wantedProfession=" + wantedProfession +
                '}';
    }
}
