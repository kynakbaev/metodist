package entity;

import java.util.Date;

/**
 * @autor Kunakbaev Artem
 */
public class Contract {
    private int id;
    private Group group;
    private Student student;
    private Profession profession;
    private Organization organization;
    private Teacher teacher;
    private Date startEducation;
    private Date endEducation;

    public Contract(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Date getStartEducation() {
        return startEducation;
    }

    public void setStartEducation(Date startEducation) {
        this.startEducation = startEducation;
    }

    public Date getEndEducation() {
        return endEducation;
    }

    public void setEndEducation(Date endEducation) {
        this.endEducation = endEducation;
    }
}
