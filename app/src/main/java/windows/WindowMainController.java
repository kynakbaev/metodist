package windows;

import data_base.DBconnection;
import data_mapper.StudentMapper;
import entity.Profession;
import entity.Student;
import facade.AppFacade;
import identify_map.FinderStudents;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import observer.Subject;

import java.sql.SQLException;

/**
 * @autor Kunakbaev Artem
 */
public class WindowMainController extends Subject{

    @FXML
    private Button buttonInsert = new Button();

    @FXML
    public Button buttonSelect = new Button();

    @FXML
    Button buttonUpdate = new Button();

    @FXML
    Button buttonDelete = new Button();

    @FXML
    private TextArea textInsert = new TextArea();

    @FXML
    private TextField textId = new TextField();

    @FXML
    private TextField textName = new TextField();

    @FXML
    private TextField textProfCode = new TextField();

    private FinderStudents finderStudents = new FinderStudents();

    public WindowMainController() {
        this.attach(new AppFacade());
    }

    @FXML
    void pressSelect(){
        Student student = finderStudents.getStudent(Integer.parseInt(textId.getText()));
        textInsert.setText(student.toString());
    }

    @FXML
    void pressInsert(){
        Student student = getStudent();
        finderStudents.addStudent(student);
        textInsert.setText("Студент добавлен");
    }

    private Student getStudent() {
        Student student = new Student();
        student.setId(Integer.parseInt(textId.getText()));
        student.setName(textName.getText());
        student.setWantedProfession(new Profession(Integer.parseInt(textProfCode.getText())));
        return student;
    }

    @FXML
    void pressUpdate() {
        Student student = getStudent();
        StudentMapper mapper = new StudentMapper(DBconnection.getConn());
        try {
            mapper.update(student.getId(), student);
            textInsert.setText("Студент с индексом " + student.getId() + " обновлен");
        } catch (SQLException e) {
            textInsert.setText(e.toString());
            e.printStackTrace();
        }
    }

    @FXML
    void pressDelete() {
        Student student = getStudent();
        StudentMapper mapper = new StudentMapper(DBconnection.getConn());
        try {
            mapper.delete(student);
            textInsert.setText("Студент с индексом " + student.getId() + " удален");
        } catch (SQLException e) {
            textInsert.setText(e.toString());
            e.printStackTrace();
        }
    }

}
