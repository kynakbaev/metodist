package facade;

import data_base.DBInsert;
import data_mapper.StudentMapper;
import observer.Observer;
import observer.Subject;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static data_base.DBconstants.COL_NAME;
import static data_base.DBconstants.TAB_STUDENTS;

/**
 * @autor Kunakbaev Artem
 */
public class AppFacade implements Observer {

    //Тут буду размещать упрощенные методы (Фасад) для работы с базой данных.
    //Пока размещен только один метод
    public void insertStudent(String name) {
        List<String> columns = new ArrayList<>();
        List<String> values = new ArrayList<>();
        columns.add(COL_NAME);
        values.add(name);
        new DBInsert(TAB_STUDENTS, columns, values);
    }

    @Override
    public void update(Subject subject, Object arg) {
        System.out.println("Пришла строка: " + arg.toString());
    }
}
