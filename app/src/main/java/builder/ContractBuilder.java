package builder;

import entity.Contract;
import entity.Organization;
import entity.Profession;
import entity.Student;

import java.util.Date;

/**
 * @autor Kunakbaev Artem
 */
public class ContractBuilder {
    private Contract contract;

    public ContractBuilder(int id) {
        contract = new Contract(id);
    }

    public ContractBuilder addStudent(Student student) {
        contract.setStudent(student);
        return this;
    }

    public ContractBuilder addProfession(Profession profession) {
        contract.setProfession(profession);
        return this;
    }

    public ContractBuilder addOrganization(Organization organization) {
        contract.setOrganization(organization);
        return this;
    }

    public ContractBuilder addStartEducation(Date date) {
        contract.setStartEducation(date);
        return this;
    }

    public ContractBuilder addEndEducation(Date date) {
        contract.setEndEducation(date);
        return this;
    }

    public Contract build() {
        return contract;
    }
}
