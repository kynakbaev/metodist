package data_mapper;

import entity.Profession;
import entity.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @autor Kunakbaev Artem
 */
public class StudentMapper {
    private final Connection connection;

    public StudentMapper(Connection connection) {
        this.connection = connection;
    }

    public Student findById(int idPerson) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT id, name, professionCode FROM metodist.students WHERE id = ?");
        statement.setInt(1, idPerson);
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt(1));
                student.setName(resultSet.getString(2));
                student.setWantedProfession(new Profession(resultSet.getInt(3)));
                return student;
            }
        }
        System.out.println("Студент не найден");
        return null;
    }

    public void insert(Student student) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "insert into `metodist`.`students` (`id`, `name`, `professionCode`) VALUES (?, ?, ?)");
        statement.setInt(1, student.getId());
        statement.setString(2, student.getName());
        statement.setInt(3, student.getWantedProfession().getCode());
        statement.execute();
    }

    public void update(int id, Student student) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "update `metodist`.`students` set `id` = ?, `name` = ?, `professionCode` = ? WHERE (`id` = ?);");
        statement.setInt(1, student.getId());
        statement.setString(2, student.getName());
        statement.setInt(3, student.getWantedProfession().getCode());
        statement.setInt(4, id);
        statement.execute();
    }

    public void delete(Student student) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                "delete from `metodist`.`students` where (`id` = ?);");
        statement.setInt(1, student.getId());
        statement.execute();
    }
}
