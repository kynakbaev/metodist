package data_base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @autor Kunakbaev Artem
 */
public class DBconnection {
    private static final String login = "root";
    private static final String pass = "12345678";
    private static final String url = "jdbc:mysql://localhost:3306/test?" +
            "&useLegacyDatetimeCode=false" +
            "&serverTimezone=UTC";
    private static Connection conn;

    static {
        try {
            conn = DriverManager.getConnection(url, login, pass);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConn() {
        return conn;
    }

}
