package data_base;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static data_base.DBconstants.DB_NAME;

/**
 * @autor Kunakbaev Artem
 */
public class DBInsert {


    public DBInsert(String table, List<String> columns, List<String> values) {

        StringBuilder insert = new StringBuilder("INSERT INTO ");
        insert.append("`").append(DB_NAME).append("`.")
                .append("`").append(table).append("` (");

        for (String column : columns) {
            insert.append("`").append(column).append("`");
        }

        insert.append(") VALUES (");

        for (String value : values) {
            insert.append("'").append(value).append("'");
        }

        insert.append(");");

        System.out.println(insert.toString());

        try {
            Statement statement = DBconnection.getConn().createStatement();
            statement.execute(insert.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
